﻿namespace FlacCue
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelInfos = new System.Windows.Forms.TableLayoutPanel();
            this.labelOccurrences = new System.Windows.Forms.Label();
            this.textBoxOccurrences = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.richTextBoxSource = new System.Windows.Forms.RichTextBox();
            this.richTextBoxPreview = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanelButtons = new System.Windows.Forms.TableLayoutPanel();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonSaveAs = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.checkBoxMessages = new System.Windows.Forms.CheckBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.tableLayoutPanelInfos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanelButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelInfos
            // 
            this.tableLayoutPanelInfos.ColumnCount = 9;
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelInfos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelInfos.Controls.Add(this.labelOccurrences, 1, 1);
            this.tableLayoutPanelInfos.Controls.Add(this.textBoxOccurrences, 3, 1);
            this.tableLayoutPanelInfos.Controls.Add(this.label1, 5, 1);
            this.tableLayoutPanelInfos.Controls.Add(this.textBoxPath, 7, 1);
            this.tableLayoutPanelInfos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanelInfos.Location = new System.Drawing.Point(0, 609);
            this.tableLayoutPanelInfos.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelInfos.Name = "tableLayoutPanelInfos";
            this.tableLayoutPanelInfos.RowCount = 3;
            this.tableLayoutPanelInfos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelInfos.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelInfos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelInfos.Size = new System.Drawing.Size(984, 44);
            this.tableLayoutPanelInfos.TabIndex = 0;
            // 
            // labelOccurrences
            // 
            this.labelOccurrences.AutoSize = true;
            this.labelOccurrences.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelOccurrences.Location = new System.Drawing.Point(25, 14);
            this.labelOccurrences.Margin = new System.Windows.Forms.Padding(0);
            this.labelOccurrences.Name = "labelOccurrences";
            this.labelOccurrences.Size = new System.Drawing.Size(224, 20);
            this.labelOccurrences.TabIndex = 0;
            this.labelOccurrences.Text = "Number of occurrences (= number of tracks):  ";
            this.labelOccurrences.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxOccurrences
            // 
            this.textBoxOccurrences.Location = new System.Drawing.Point(259, 14);
            this.textBoxOccurrences.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxOccurrences.Name = "textBoxOccurrences";
            this.textBoxOccurrences.ReadOnly = true;
            this.textBoxOccurrences.Size = new System.Drawing.Size(35, 20);
            this.textBoxOccurrences.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(314, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Path:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPath
            // 
            this.textBoxPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPath.Location = new System.Drawing.Point(356, 14);
            this.textBoxPath.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.ReadOnly = true;
            this.textBoxPath.Size = new System.Drawing.Size(608, 20);
            this.textBoxPath.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.richTextBoxSource);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.richTextBoxPreview);
            this.splitContainer1.Size = new System.Drawing.Size(984, 609);
            this.splitContainer1.SplitterDistance = 494;
            this.splitContainer1.TabIndex = 1;
            // 
            // richTextBoxSource
            // 
            this.richTextBoxSource.BackColor = System.Drawing.Color.White;
            this.richTextBoxSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxSource.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxSource.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxSource.Margin = new System.Windows.Forms.Padding(0);
            this.richTextBoxSource.Name = "richTextBoxSource";
            this.richTextBoxSource.ReadOnly = true;
            this.richTextBoxSource.Size = new System.Drawing.Size(494, 609);
            this.richTextBoxSource.TabIndex = 0;
            this.richTextBoxSource.Text = "";
            this.richTextBoxSource.Resize += new System.EventHandler(this.richTextBox_Resize);
            // 
            // richTextBoxPreview
            // 
            this.richTextBoxPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxPreview.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxPreview.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxPreview.Margin = new System.Windows.Forms.Padding(0);
            this.richTextBoxPreview.Name = "richTextBoxPreview";
            this.richTextBoxPreview.Size = new System.Drawing.Size(486, 609);
            this.richTextBoxPreview.TabIndex = 0;
            this.richTextBoxPreview.Text = "";
            this.richTextBoxPreview.Resize += new System.EventHandler(this.richTextBox_Resize);
            // 
            // tableLayoutPanelButtons
            // 
            this.tableLayoutPanelButtons.ColumnCount = 11;
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelButtons.Controls.Add(this.buttonExit, 9, 1);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonSaveAs, 5, 1);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonSave, 3, 1);
            this.tableLayoutPanelButtons.Controls.Add(this.checkBoxMessages, 1, 1);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonClear, 7, 1);
            this.tableLayoutPanelButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanelButtons.Location = new System.Drawing.Point(0, 653);
            this.tableLayoutPanelButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelButtons.Name = "tableLayoutPanelButtons";
            this.tableLayoutPanelButtons.RowCount = 3;
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelButtons.Size = new System.Drawing.Size(984, 49);
            this.tableLayoutPanelButtons.TabIndex = 1;
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExit.Location = new System.Drawing.Point(889, 10);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(0);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 0;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonSaveAs
            // 
            this.buttonSaveAs.Location = new System.Drawing.Point(664, 10);
            this.buttonSaveAs.Margin = new System.Windows.Forms.Padding(0);
            this.buttonSaveAs.Name = "buttonSaveAs";
            this.buttonSaveAs.Size = new System.Drawing.Size(130, 23);
            this.buttonSaveAs.TabIndex = 0;
            this.buttonSaveAs.Text = "Save CUE (FLAC) as...";
            this.buttonSaveAs.UseVisualStyleBackColor = true;
            this.buttonSaveAs.Click += new System.EventHandler(this.buttonSaveAs_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(524, 10);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(0);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(130, 23);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "Save CUE (FLAC)";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // checkBoxMessages
            // 
            this.checkBoxMessages.AutoSize = true;
            this.checkBoxMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxMessages.Location = new System.Drawing.Point(20, 10);
            this.checkBoxMessages.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxMessages.Name = "checkBoxMessages";
            this.checkBoxMessages.Size = new System.Drawing.Size(103, 23);
            this.checkBoxMessages.TabIndex = 2;
            this.checkBoxMessages.Text = "Show messages";
            this.checkBoxMessages.UseVisualStyleBackColor = true;
            // 
            // buttonClear
            // 
            this.buttonClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClear.Location = new System.Drawing.Point(804, 10);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 0;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExit;
            this.ClientSize = new System.Drawing.Size(984, 702);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.tableLayoutPanelInfos);
            this.Controls.Add(this.tableLayoutPanelButtons);
            this.Name = "Form1";
            this.Text = "FlacCue (v0.5)";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            this.tableLayoutPanelInfos.ResumeLayout(false);
            this.tableLayoutPanelInfos.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanelButtons.ResumeLayout(false);
            this.tableLayoutPanelButtons.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelInfos;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox richTextBoxSource;
        private System.Windows.Forms.RichTextBox richTextBoxPreview;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelButtons;
        private System.Windows.Forms.Label labelOccurrences;
        private System.Windows.Forms.TextBox textBoxOccurrences;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonSaveAs;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.CheckBox checkBoxMessages;
        private System.Windows.Forms.Button buttonClear;


    }
}

