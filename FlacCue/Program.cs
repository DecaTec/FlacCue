﻿using System;
using System.Windows.Forms;
using System.IO;

namespace FlacCue
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string startupFile = String.Empty;

            if (args != null && args.Length > 0 && !String.IsNullOrEmpty(args[0]) 
                && args[0].ToLower().EndsWith(".cue") && File.Exists(args[0]))
                startupFile = args[0];

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(startupFile));
        }
    }
}
