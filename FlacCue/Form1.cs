﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace FlacCue
{
    public partial class Form1 : Form
    {
        private FileInfo originalFileInfo;
        private string startupFileName;

        // WAV extension in CUE sheets.
        private static string WAV_STR = ".wav\" WAVE";    
        // FLAC extension in CUE sheets.
        private static string FLAC_STR = ".flac\" WAVE";
        private static string FILE_LINE_BEG = "FILE \"";
        private static string TITLE_SONG_LINE_BEG = "    TITLE \"";
        private static string TITLE_ALBUM_LINE_BEG = "TITLE \"";
        private static string EMPTY_COMPOSER = "REM COMPOSER \"\"";

        public Form1(string startupFileName)
        {
            if (!String.IsNullOrEmpty(startupFileName))
            {
                LoadAndSaveWithoutUI(startupFileName);
                Environment.Exit(0);
            }

            this.startupFileName = startupFileName;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.splitContainer1.SplitterDistance = this.Width / 2;

            if (!String.IsNullOrEmpty(this.startupFileName))
            {
                this.originalFileInfo = new FileInfo(this.startupFileName);
                LoadCue(this.startupFileName);
            }
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            bool isCorrect = false;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filenames = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (filenames.Length > 1)
                {
                    isCorrect = false;
                }
                else
                {
                    if (!File.Exists(filenames[0]))
                        isCorrect = false;
                    else
                    {
                        FileInfo fi = new FileInfo(filenames[0]);
                        this.originalFileInfo = fi;

                        if (fi.Extension.ToLower() == ".cue")
                            isCorrect = true;
                    }
                }
            }

            if (isCorrect)
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            ClearUI();
            string[] fileNames = (string[])(e.Data.GetData(DataFormats.FileDrop, true));

            // When more than one file is dragged over the window, this event is never thrown.
            LoadCue(fileNames[0]);
        }

        private void ClearUI()
        {
            this.richTextBoxPreview.Text = String.Empty;
            this.richTextBoxSource.Text = String.Empty;
            this.textBoxOccurrences.Text = String.Empty;
            this.textBoxPath.Text = String.Empty;
        }

        private void LoadCue(string fileName)
        {
            string str = File.ReadAllText(fileName, Encoding.Default);
            this.richTextBoxSource.Text = str;
            this.textBoxPath.Text = fileName;

            if (!MarkSource())
                return;

            this.richTextBoxPreview.Text = ReplaceWavWithFlac(this.richTextBoxSource.Text);
            MarkAllOccurenciesOfText(this.richTextBoxPreview, FLAC_STR.Remove(0,1), 4, Color.LightGreen);
        }

        private void LoadAndSaveWithoutUI(string fileName)
        {
            string str = File.ReadAllText(fileName, Encoding.Default);
            string flacString = ReplaceWavWithFlac(str);
            string newString = RemoveEmptyComposer(flacString);
            newString = RemoveFullPaths(newString);
            newString = SongTitlesToUpper(newString);
            SaveCueWithoutUI(fileName, newString);
        }

        private bool MarkSource()
        {
            int count = MarkAllOccurenciesOfText(this.richTextBoxSource, WAV_STR.Remove(0,1), 3, Color.Red);
            this.textBoxOccurrences.Text = count.ToString();

            return count > 0;
        }

        private int[] FindIndices(string sourceText, string textToFind)
        {
            IList<int> indices = new List<int>();
            int index = -1;

            while ((index = sourceText.IndexOf(textToFind, index + 1)) != -1)
            {
                indices.Add(index);
            }

            int[] iArr = new int[indices.Count];

            for (int i = 0; i < indices.Count; i++)
            {
                iArr[i] = indices[i];
            }

            return iArr;
        }

        private int MarkAllOccurenciesOfText(RichTextBox richTextBox, string textToMark, int markLength, Color markColor)
        {
            string str = richTextBox.Text;
            int[] indices = FindIndices(str, textToMark);

            for (int i = 0; i < indices.Length; i++)
            {
                richTextBox.Select(indices[i], markLength);
                richTextBox.SelectionBackColor = markColor;
            }

            richTextBox.Select(0, 0);
            return indices.Length;
        }

        private string ReplaceWavWithFlac(string str)
        {
            return str.Replace(WAV_STR, FLAC_STR);
        }

        private string RemoveEmptyComposer(string str)
        {
            return str.Replace(EMPTY_COMPOSER, "");
        }

        private string RemoveFullPaths(string content)
        {
            var sBuilder = new StringBuilder();
            string[] contentArr = content.Split(Environment.NewLine.ToCharArray());
            string tmp;

            foreach (var line in contentArr)
            {
                if(line.StartsWith(FILE_LINE_BEG))
                {
                    var idx = line.LastIndexOf("\\");

                    if (idx != -1)
                        tmp = line.Remove(6, idx - 5);
                    else
                        tmp = line;

                    sBuilder.AppendLine(tmp);
                    continue;
                }

                sBuilder.AppendLine(line);
            }

            return sBuilder.ToString();
        }

        private string SongTitlesToUpper(string content)
        {            
            var sBuilder = new StringBuilder();
            string[] contentArr = content.Split(Environment.NewLine.ToCharArray());
            string tmp;

            foreach (var line in contentArr)
            {
                if(line.StartsWith(FILE_LINE_BEG) || line.StartsWith(TITLE_SONG_LINE_BEG) || line.StartsWith(TITLE_ALBUM_LINE_BEG))
                {
                    String newLine = String.Empty;
                    var sArr = line.Split('"');
                    newLine = sArr[0];

                    String songTitle = sArr[1];
                    var sArr2 = songTitle.Split(' ');
                    newLine = newLine + "\"";

                    for (int i = 0; i < sArr2.Length; i++)
                    {
                        if (i == 0)
                            newLine = newLine + sArr2[i].First().ToString().ToUpper() + sArr2[i].Substring(1);
                        else
                            newLine = newLine + " " + sArr2[i].First().ToString().ToUpper() + sArr2[i].Substring(1);
                    }

                    newLine = newLine + "\"";
                    newLine = newLine + sArr[2];	

                    sBuilder.AppendLine(newLine);
                    continue;
                }

                sBuilder.AppendLine(line);
            }

            return sBuilder.ToString();
        }

        private void SaveCue(string fileName, string content)
        {
            FileStream fStream = null;
            StreamWriter writer = null;

            string[] contentArr = content.Split(Environment.NewLine.ToCharArray());

            try
            {
                using (fStream = File.Open(fileName, FileMode.Create))
                {
                    using (writer = new StreamWriter(fStream, Encoding.Default))
                    {
                        for (int i = 0; i < contentArr.Length; i++)
                        {
                            if(!String.IsNullOrEmpty(contentArr[i]))
                                writer.WriteLine(contentArr[i]);
                        }
                    }
                }

                if (this.checkBoxMessages.Checked)
                {
                    DialogResult result = MessageBox.Show("CUE sheet created:\n" + fileName + "\n\nOpen in assoiciated program?", "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                        Process.Start(fileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errror", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveCueWithoutUI(string fileName, string content)
        {
            FileStream fStream = null;
            StreamWriter writer = null;

            string[] contentArr = content.Split(Environment.NewLine.ToCharArray());

            try
            {
                using (fStream = File.Open(fileName, FileMode.Create))
                {
                    using (writer = new StreamWriter(fStream, Encoding.Default))
                    {
                        for (int i = 0; i < contentArr.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(contentArr[i]) && contentArr[i].Any(x => char.IsLetterOrDigit(x)))
                                writer.WriteLine(contentArr[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonSaveAs_Click(object sender, EventArgs e)
        {
            if (this.originalFileInfo == null || String.IsNullOrEmpty(this.richTextBoxPreview.Text))
            {
                if(this.checkBoxMessages.Checked)
                    MessageBox.Show("No CUE sheet available for saving.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }

            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "CUE sheets (*.cue)|*.cue|All files (*.*)|*.*";
            saveDlg.OverwritePrompt = true;
            saveDlg.DefaultExt = ".cue";
            saveDlg.AddExtension = true;
            saveDlg.InitialDirectory = this.originalFileInfo.Directory.FullName;
            saveDlg.FileName = this.originalFileInfo.FullName;

            if (saveDlg.ShowDialog() == DialogResult.OK)
            {
                SaveCue(saveDlg.FileName, this.richTextBoxPreview.Text);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (this.originalFileInfo == null || String.IsNullOrEmpty(this.richTextBoxPreview.Text))
            {
                if (this.checkBoxMessages.Checked)
                    MessageBox.Show("No CUE sheet available for saving.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }

            SaveCue(this.originalFileInfo.FullName, this.richTextBoxPreview.Text);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearUI();
        }

        private void richTextBox_Resize(object sender, EventArgs e)
        {
            this.richTextBoxSource.Invalidate();
            this.richTextBoxPreview.Invalidate();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
